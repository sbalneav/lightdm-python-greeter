# lightdm-python-greeter

lightdm-python-greeter is a greeter that properly implements full pam semantics.  Most greeters out there look very nice, but fail horribly if you have to do periodic password expiry, want to insert the pam-passwdqc module.